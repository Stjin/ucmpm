#nullable enable
using System.Collections.Generic;

namespace UCMPM.Data
{
    public class ListInboundRoutesResponse
    {
        // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
        public class Member
        {
            public string inbound_mode { get; set; }
            public string tc { get; set; }
            public string external_number { get; set; }
            public string destination_type { get; set; }
            public string account { get; set; }
        }

        public class InboundRoute
        {
            public int inbound_rt_index { get; set; }
            public string? enable_inbound_muti_mode { get; set; } // Enable Route-Level Inbound Mode
            public string? en_multi_mode { get; set; } // Inbound Multiple Mode
            public string? inbound_rt_name { get; set; }

            // UCM 6202
            public string? did_pattern_match { get; set; }

            // UCM 6301
            public string? did_pattern_match_list { get; set; }
            public List<Member>? members { get; set; }
            public int inbound_mode { get; set; }
        }

        public class Response
        {
            public List<InboundRoute> inbound_route { get; set; }
        }

        public class Root
        {
            public Response response { get; set; }
            public int status { get; set; }
        }
    }


    // Make request parameters nullable to prevent default values (such as 0) to be accidentally sent to the API endpoint
    internal class ListInboundRoutesRequest
    {
        // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
        public class Request
        {
            public string? action { get; set; }
            public string? cookie { get; set; }

            // trunk_index was added in the 1.0.9.8 BETA but was never documented in the changelog.
            // The API documentation state this has to be an int, but apparently sending an empty string returns all the routes regardless of the trunk index(???)
            public string? trunk_index { get; set; } = "";
        }

        public class Root
        {
            public Request request { get; set; }
        }
    }

    // Make request parameters nullable to prevent default values (such as 0) to be accidentally sent to the API endpoint
    internal class UpdateInboundRoutesRequest
    {
        // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 

        public class MultiMode
        {
            public string? mode { get; set; }
            public string? destination_type { get; set; }
            public string? external_number { get; set; }
            public string? account { get; set; }
        }
        public class Request
        {
            public string? action { get; set; }
            public string? cookie { get; set; }
            public int? inbound_muti_mode { get; set; }
            public int? inbound_route { get; set; }
            public List<MultiMode>? multi_mode { get; set; }

        }

        public class Root
        {
            public Request request { get; set; }
        }
    }

    internal class UpdateInboundRoutesResponse
    {
        // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
        public class Response
        {
            public string need_apply { get; set; }
        }

        public class Root
        {
            public Response response { get; set; }
            public int status { get; set; }
        }
    }
}