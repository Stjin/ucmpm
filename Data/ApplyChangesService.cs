﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Newtonsoft.Json;
using RestSharp;

namespace UCMPM.Data
{
    public class ApplyChangesService
    {
        private UcmSettings UcmSettings { get; }

        public ApplyChangesService(UcmSettings ucmSettings)
        {
            UcmSettings = ucmSettings;
        }

        public async Task<bool> ApplyChangesAsync(string cookie)
        {
            Helper.WriteLogToEvents($"ApplyChangesAsync(); Applying changes for host {UcmSettings.SessionSettings.host}...", EventLogEntryType.Information);

            try
            {
                var applyChangesRequest = new ApplyChangesRequest.Root
                {
                    request = new ApplyChangesRequest.Request
                    {
                        action = "applyChanges",
                        cookie = cookie
                    }
                };

                var options = new RestClientOptions($"{ UcmSettings.SessionSettings.host }:{ UcmSettings.SessionSettings.ucmPort}/api")
                {
                    MaxTimeout = -1,
                    RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true
                };

                var client = new RestClient(options);


                // Disable SSL checks as UCMSettings does not have a SSL cert by default

                var request = new RestRequest();
                request.AddHeader("Content-Type", "application/json;charset=UTF-8");
                request.AddHeader("Connection", "close");

                // Serialize the data request into a json and add it as body
                request.AddJsonBody(applyChangesRequest);
                var response = await client.PostAsync(request);
                var applyChangesResponse = JsonConvert.DeserializeObject<ApplyChangesResponse.Root>(response.Content);
                if (applyChangesResponse?.status == 0)
                {
                    return true;
                }

                Helper.WriteLogToEvents($"ApplyChangesAsync(); applyChangesResponse was {JsonConvert.SerializeObject(applyChangesResponse)}");
                return false;
            }
            catch (Exception e)
            {
                Helper.WriteLogToEvents($"ApplyChangesAsync(); {e.Message}");
                return false;
            }
        }
    }
}