﻿using System;
using System.Diagnostics;
using System.Text.RegularExpressions;

namespace UCMPM.Data
{
    public class Helper
    {
        public static bool ValidatePhone(string phone)
        {
            var phoneRegex = new Regex(@"^\d{10}$");
            return phoneRegex.IsMatch(phone);
        }


        // Remove all special characters from a string
        public static string CleanString(string text)
        {
            Regex reg = new(@"[^0-9A-Za-z ,]");
            return reg.Replace(text, string.Empty);
        }

        public static string FixNumberSyntax(string externalNumber)
        {
            // Remove all whitespaces
            // Remove (
            // Remove )
            // Replace +31 with 0's
            return externalNumber.Replace(" ", "").Replace("(", "").Replace(")", "").Replace("+31", "0");
        }

        // The default event is a warning
        public static bool WriteLogToEvents(string error, EventLogEntryType entryType = EventLogEntryType.Warning)
        {
            try
            {
                string source = "UCMPM";
                string log = "Application";
                if (!EventLog.SourceExists(source))
                {
                    EventLog.CreateEventSource(source, log);
                }
                EventLog.WriteEntry(source, error,
                    entryType, 5151);

                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine($"Sigh... people do not read readme files. Anyways, here is the exception: {e.Message}");
                return false;
            }


        }
    }
}
