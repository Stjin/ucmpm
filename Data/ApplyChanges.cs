﻿namespace UCMPM.Data
{
    public class ApplyChangesResponse
    {
        // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
        public class Response
        {
            public string? settings { get; set; }
        }

        public class Root
        {
            public Response? response { get; set; }
            public int? status { get; set; }
        }
    }

    internal class ApplyChangesRequest
    {
        // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
        public class Request
        {
            public string action { get; set; }
            public string cookie { get; set; }
        }

        public class Root
        {
            public Request request { get; set; }
        }
    }
}