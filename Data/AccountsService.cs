using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using RestSharp;

namespace UCMPM.Data
{
    /*
     * GET THE ACCOUNTS (EXTENSIONS)
     */
    public class AccountsService
    {
        private UcmSettings UcmSettings { get; }

        public AccountsService(UcmSettings ucmSettings)
        {
            UcmSettings = ucmSettings;
        }

        // This method just takes the accounts starting with "VM-"
        public async Task<List<ListAccountsResponse.Account>?> GetVoicemailAccountsAsync(string cookie)
        {
            var accounts = await GetAccountsAsync(cookie);

            // Return only the accounts starting with "VM-"
            return accounts?.Where(account => account.fullname != null && account.fullname.StartsWith("VM-")).ToList();

        }

        private async Task<List<ListAccountsResponse.Account>?> GetAccountsAsync(string cookie)
        {
            try
            {
                var accountsRequest = new ListAccountsRequest.Root
                {
                    request = new ListAccountsRequest.Request
                    {
                        action = "listAccount",
                        cookie = cookie,
                        options = "extension,fullname"
                    }
                };


                var options = new RestClientOptions($"{ UcmSettings.SessionSettings.host }:{ UcmSettings.SessionSettings.ucmPort}/api")
                {
                    MaxTimeout = -1,
                    RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true
                };

                var client = new RestClient(options);


                // Disable SSL checks as UCMSettings does not have a SSL cert by default

                var request = new RestRequest();
                request.AddHeader("Content-Type", "application/json;charset=UTF-8");
                request.AddHeader("Connection", "close");

                // Serialize the data request into a json and add it as body
                request.AddJsonBody(accountsRequest);
                var response = await client.PostAsync(request);
                var accountsResponse =
                    JsonConvert.DeserializeObject<ListAccountsResponse.Root>(response.Content);


                return accountsResponse?.response?.account;
            }
            catch (Exception e)
            {
                Helper.WriteLogToEvents($"GetAccountsAsync(); {e.Message}");

                //TODO Fix this dirty as hell way of showing the error to the user.
                //Anyways, this error is being returned if something goes wrong while obtaining the accounts.
                return new List<ListAccountsResponse.Account>
                {
                 new()
                 {
                     extension = "0000",
                     fullname = "Something went wrong, please consult the event logs (EventID 5151)"
                 }
                };
            }
        }

    }
}