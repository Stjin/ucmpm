﻿using System.Collections.Generic;
using Microsoft.Extensions.Localization;
using UCMPM.Resources;

namespace UCMPM.Data
{
    public class Destinations
    {
        public class DestinationType
        {
            public string destination_type { get; set; }
            public string? destination_type_string { get; set; }
        }


        public static List<DestinationType> DestinationTypes(IStringLocalizer<SharedResource> languageContainer)
        {
            return new()
            {
                new()
                {
                    destination_type = "account",
                    destination_type_string = languageContainer["account"]
                },
                new()
                {
                    destination_type = "external_number",
                    destination_type_string = languageContainer["external_number"]
                }
            };
        }
    }
}