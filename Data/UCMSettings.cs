﻿using System;
using System.Threading.Tasks;
using Blazored.LocalStorage;

namespace UCMPM.Data
{
    public class UcmSettings
    {
        public Ucm SessionSettings = new();
        public UcmPersonalSettings PersonalSessionSettings = new();

        public async Task<bool> LoadUcmSettingsToMemory(ILocalStorageService localStore)
        {

            // HUH? Why are you storing these settings plain text?
            // Well, using ProtectedLocalStorage would be a great option in terms of security, but that means that moving this application to a different server would break localStorage on all devices
            // because the Data Protection key cannot be moved

            try
            {
                var ucmSettings = await localStore.GetItemAsync<Ucm>("settings");
                if (ucmSettings == null) return false;
                SessionSettings.host = ucmSettings.host;
                SessionSettings.ucmPort = ucmSettings.ucmPort;
                SessionSettings.username = ucmSettings.username;
                SessionSettings.password = ucmSettings.password;
                return true;
            }
            catch (Exception e)
            {
                Helper.WriteLogToEvents("LoadUcmSettingsToMemory(); Could not load values from localstorage. Clearing data");
                Helper.WriteLogToEvents($"LoadUcmSettingsToMemory(); {e.Message}");
                await localStore.ClearAsync();
                return false;
            }
        }

        public async Task<bool> SetUcmSettingsAsync(ILocalStorageService localStore, Ucm ucmSettings)
        {
            // Store values in localStore
            await localStore.SetItemAsync("settings", ucmSettings);

            // After that, reload values into memory
            await LoadUcmSettingsToMemory(localStore);
            return true;
        }


        public async Task<bool> LoadPersonalUcmSettingsToMemory(ILocalStorageService localStore)
        {

            // HUH? Why are you storing these settings plain text?
            // Well, using ProtectedLocalStorage would be a great option in terms of security, but that means that moving this application to a different server would break localStorage on all devices
            // because the Data Protection key cannot be moved

            try
            {
                var ucmPersonalSettings = await localStore.GetItemAsync<UcmPersonalSettings>("personalSettings");
                if (ucmPersonalSettings == null) return false;
                PersonalSessionSettings.hideNotConfiguredRoutes = ucmPersonalSettings.hideNotConfiguredRoutes;
                return true;
            }
            catch (Exception e)
            {
                Helper.WriteLogToEvents("LoadPersonalUcmSettingsToMemory(); Could not load values from localstorage. Clearing data");
                Helper.WriteLogToEvents($"LoadPersonalUcmSettingsToMemory(); {e.Message}");
                await localStore.ClearAsync();
                return false;
            }
        }

        public async Task<bool> SetPersonalUcmSettingsAsync(ILocalStorageService localStore, UcmPersonalSettings ucmPersonalSettings)
        {
            // Store values in localStore
            await localStore.SetItemAsync("personalSettings", ucmPersonalSettings);

            // After that, reload values into memory
            await LoadPersonalUcmSettingsToMemory(localStore);
            return true;
        }
    }

    public class Ucm
    {
        public string username { get; set; } = "";
        public string password { get; set; } = "";
        public string host { get; set; } = "";
        public string ucmPort { get; set; } = "";
    }




    public class UcmPersonalSettings
    {
        public bool hideNotConfiguredRoutes { get; set; } = true;

    }
}