﻿using System.Collections.Generic;

namespace UCMPM.Data
{
    public class ListAccountsResponse
    {
        // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
        public class Account
        {
            public string? extension { get; set; }
            public string? fullname { get; set; }
        }

        public class Response
        {
            public List<Account>? account { get; set; }
            public int total_item { get; set; }
            public int total_page { get; set; }
            public int page { get; set; }
            public int total_account_item { get; set; }
        }

        public class Root
        {
            public Response? response { get; set; }
            public int status { get; set; }
        }
    }

    public class ListAccountsRequest
    {
        // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
        public class Request
        {
            public string action { get; set; }
            public string cookie { get; set; }
            public string options { get; set; }
        }

        public class Root
        {
            public Request request { get; set; }
        }

    }



}
