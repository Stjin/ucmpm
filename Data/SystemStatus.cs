﻿using Newtonsoft.Json;

namespace UCMPM.Data
{
    public class SystemStatusResponse
    {
        // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
        public class Response
        {
            [JsonProperty("part-number")] public string PartNumber { get; set; }

            [JsonProperty("serial-number")] public string SerialNumber { get; set; }

            [JsonProperty("up-time")] public string UpTime { get; set; }

            [JsonProperty("idle-time")] public string IdleTime { get; set; }

            [JsonProperty("system-time")] public string SystemTime { get; set; }

            public string mac { get; set; }
        }

        public class Root
        {
            public Response? response { get; set; }
            public int status { get; set; }
        }
    }

    internal class SystemStatusRequest
    {
        // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
        public class Request
        {
            public string action { get; set; }
            public string cookie { get; set; }
        }

        public class Root
        {
            public Request request { get; set; }
        }
    }
}