﻿namespace UCMPM.Data
{
    public class ChallengeResponse
    {
        // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
        public class Response
        {
            public string? challenge { get; set; }
        }

        public class Root
        {
            public Response? response { get; set; }
            public int? status { get; set; }
        }
    }

    internal class ChallengeRequest
    {
        // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
        public class Request
        {
            public string action { get; set; }
            public string user { get; set; }
            public string version { get; set; }
        }

        public class Root
        {
            public Request request { get; set; }
        }
    }
}