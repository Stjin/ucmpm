﻿using System;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Blazored.LocalStorage;
using Newtonsoft.Json;
using RestSharp;


namespace UCMPM.Data
{
    public class Authenticate
    {
        public UcmSettings UcmSettings { get; }

        public Authenticate(UcmSettings ucmSettings)
        {
            UcmSettings = ucmSettings;
        }


        public async Task<string> GetCookieAsync(ILocalStorageService localStore)
        {
            var cookie = await localStore.GetItemAsync<string>("cookie");
            if (cookie != null)
            {
                return cookie;
            }
            else
            {
                Helper.WriteLogToEvents("GetCookieAsync(); cookie was null");
                return "";
            }
        }


        public async Task<string> AuthenticateOnUCMAsync(ILocalStorageService localStore)
        {
            try
            {
                var challengeResponse = GetChallenge();
                if (challengeResponse == null) return "Are the host and port correct?";
                var authCookie = GetAuthCookie(challengeResponse);
                if (authCookie == null) return "Authentication failed. Are the username and password correct?";

                // Set cookie to localstorage
                if (authCookie.response?.cookie != null)
                {
                    await localStore.SetItemAsync("cookie", authCookie.response?.cookie!);
                    return "0";
                }

                Helper.WriteLogToEvents(
                    $"AuthenticateOnUCMAsync(); authCookie.response?.cookie was {authCookie.response}");
                return "The cookie could not be obtained";

            }
            catch (Exception e)
            {
                Helper.WriteLogToEvents($"AuthenticateOnUCMAsync(); {e.Message}");
                return "Something went wrong, please consult the event logs (EventID 5151)";
            }
        }

        private ChallengeResponse.Root? GetChallenge()
        {
            try
            {
                var challengeRequest = new ChallengeRequest.Root
                {
                    request = new ChallengeRequest.Request
                    {
                        action = "challenge",
                        user = Helper.CleanString(UcmSettings.SessionSettings.username),
                        version = "1.0"
                    }
                };


                var options = new RestClientOptions($"{ UcmSettings.SessionSettings.host }:{ UcmSettings.SessionSettings.ucmPort}/api")
                {
                    MaxTimeout = -1,
                    RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true
                };

                var client = new RestClient(options);

                // Disable SSL checks as UCMSettings does not have a SSL cert by default

                var request = new RestRequest();
                request.AddHeader("Content-Type", "application/json;charset=UTF-8");
                request.AddHeader("Connection", "close");

                // Serialize the data request into a json and add it as body
                request.AddJsonBody(challengeRequest);
                var response = client.PostAsync(request);
                if (response.Result.Content == null) return null;
                var challengeResponse = JsonConvert.DeserializeObject<ChallengeResponse.Root>(response.Result.Content);
                if (challengeResponse?.status == 0)
                {
                    return challengeResponse;
                }

                Helper.WriteLogToEvents(
                    $"GetChallenge(); challengeResponseStatus was {challengeResponse?.status} instead of 0");

                return null;
            }
            catch (Exception e)
            {
                Helper.WriteLogToEvents($"GetChallenge(); {e.Message}");
                return null;
            }
        }

        private AuthCookieResponse.Root? GetAuthCookie(ChallengeResponse.Root challengeResponse)
        {
            try
            {
                var authCookieRequest = new AuthCookieRequest.Root
                {
                    request = new AuthCookieRequest.Request
                    {
                        action = "login",
                        token = CreateMD5Hash(
                            $"{challengeResponse.response?.challenge}{UcmSettings.SessionSettings.password}"),
                        user = Helper.CleanString(UcmSettings.SessionSettings.username)
                    }
                };



                var options = new RestClientOptions($"{UcmSettings.SessionSettings.host}:{UcmSettings.SessionSettings.ucmPort}/api")
                {
                    RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true,
                    MaxTimeout = -1
                };

                var client = new RestClient(options);

                // Disable SSL checks as UCMSettings does not have a SSL cert by default

                var request = new RestRequest();
                request.AddHeader("Content-Type", "application/json;charset=UTF-8");
                request.AddHeader("Connection", "close");

                // Serialize the data request into a json and add it as body
                request.AddJsonBody(authCookieRequest);
                var response = client.PostAsync(request);
                if (response.Result.Content == null) return null;
                var authCookieResponse = JsonConvert.DeserializeObject<AuthCookieResponse.Root>(response.Result.Content);
                if (authCookieResponse?.status == 0)
                {
                    return authCookieResponse;
                }

                Helper.WriteLogToEvents(
                    $"GetAuthCookie(); authCookieResponse was {JsonConvert.SerializeObject(authCookieResponse)}");

                return null;
            }
            catch (Exception e)
            {
                Helper.WriteLogToEvents($"GetAuthCookie(); {e.Message}");
                return null;
            }
        }

        private string CreateMD5Hash(string input)
        {
            // Step 1, calculate MD5 hash from input
            var md5 = MD5.Create();
            var inputBytes = Encoding.ASCII.GetBytes(input);
            var hashBytes = md5.ComputeHash(inputBytes);

            // Step 2, convert byte array to hex string
            var sb = new StringBuilder();
            foreach (var t in hashBytes) sb.Append(t.ToString("X2"));
            return sb.ToString().ToLower();
        }
    }
}