﻿using System;
using System.Threading.Tasks;
using Newtonsoft.Json;
using RestSharp;

namespace UCMPM.Data
{
    public class SystemStatusService
    {
        private UcmSettings UcmSettings { get; }

        public SystemStatusService(UcmSettings ucmSettings)
        {
            UcmSettings = ucmSettings;
        }
        public async Task<SystemStatusResponse.Response?> ObtainSystemStatus(string cookie)
        {
            try
            {
                var updateInboundRoutesRequest = new SystemStatusRequest.Root
                {
                    request = new SystemStatusRequest.Request
                    {
                        action = "getSystemStatus",
                        cookie = cookie
                    }
                };


                var options = new RestClientOptions($"{ UcmSettings.SessionSettings.host }:{ UcmSettings.SessionSettings.ucmPort}/api")
                {
                    MaxTimeout = -1,
                    RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true
                };

                var client = new RestClient(options);


                // Disable SSL checks as UCMSettings does not have a SSL cert by default

                var request = new RestRequest();
                request.AddHeader("Content-Type", "application/json;charset=UTF-8");
                request.AddHeader("Connection", "close");

                // Serialize the data request into a json and add it as body
                request.AddJsonBody(updateInboundRoutesRequest);
                var response = await client.PostAsync(request);
                var systemStatusResponse = JsonConvert.DeserializeObject<SystemStatusResponse.Root>(response.Content);
                return systemStatusResponse?.response;
            }
            catch (Exception e)
            {
                Helper.WriteLogToEvents($"ObtainSystemStatus(); {e.Message}");
                throw;
            }

        }
    }
}