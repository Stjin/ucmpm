﻿namespace UCMPM.Data
{
    public class AuthCookieResponse
    {
        // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
        public class Response
        {
            public string? cookie { get; set; }
        }

        public class Root
        {
            public Response? response { get; set; }
            public int? status { get; set; }
        }
    }

    internal class AuthCookieRequest
    {
        // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
        public class Request
        {
            public string action { get; set; }
            public string token { get; set; }
            public string user { get; set; }
        }

        public class Root
        {
            public Request request { get; set; }
        }
    }
}