using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Blazored.LocalStorage;
using Microsoft.AspNetCore.Components;
using Newtonsoft.Json;
using RestSharp;
using static System.Runtime.InteropServices.JavaScript.JSType;
using static UCMPM.Data.ListInboundRoutesResponse;

namespace UCMPM.Data
{
    public class InboundRoutesService
    {
        private UcmSettings UcmSettings { get; }

        private readonly AccountsService _accountsService;

        public InboundRoutesService(UcmSettings ucmSettings, AccountsService accountsService)
        {
            UcmSettings = ucmSettings;
            _accountsService = accountsService;

        }

        public async Task<List<ListInboundRoutesResponse.InboundRoute>> GetInboundRoutesAsync(string cookie)
        {
            try
            {
                var inboundRoutesRequest = new ListInboundRoutesRequest.Root
                {
                    request = new ListInboundRoutesRequest.Request
                    {
                        action = "listInboundRoute",
                        cookie = cookie
                    }
                };


                var options =
                    new RestClientOptions(
                        $"{UcmSettings.SessionSettings.host}:{UcmSettings.SessionSettings.ucmPort}/api")
                    {
                        MaxTimeout = -1,
                        RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true
                    };

                var client = new RestClient(options);


                // Disable SSL checks as UCMSettings does not have a SSL cert by default

                var request = new RestRequest();
                request.AddHeader("Content-Type", "application/json;charset=UTF-8");
                request.AddHeader("Connection", "close");

                // Serialize the data request into a json and add it as body
                request.AddJsonBody(inboundRoutesRequest);
                var response = await client.PostAsync(request);
                var inboundRoutesResponse =
                    JsonConvert.DeserializeObject<ListInboundRoutesResponse.Root>(response.Content);


                return inboundRoutesResponse.response.inbound_route;
            }
            catch (Exception e)
            {
                Helper.WriteLogToEvents($"GetInboundRoutesAsync(); {e.Message}");

                //TODO Fix this dirty as hell way of showing the error to the user.
                //Anyways, this error is being returned if something goes wrong while obtaining the routes.
                return new List<ListInboundRoutesResponse.InboundRoute>
                {
                    new()
                    {
                        inbound_rt_name = "ERROR",
                        en_multi_mode = "no",
                        enable_inbound_muti_mode = "no",
                        did_pattern_match_list = "Something went wrong, please consult the event logs (EventID 5151)"
                    }
                };
            }
        }

        public bool SetInboundMutiMode(string cookie, int mode, int routeIndex)
        {
            try
            {
                var updateInboundRoutesRequest = new UpdateInboundRoutesRequest.Root
                {
                    request = new UpdateInboundRoutesRequest.Request
                    {
                        inbound_route = routeIndex,
                        action = "updateInboundRoute",
                        cookie = cookie,
                        inbound_muti_mode = mode
                    }
                };


                var options =
                    new RestClientOptions(
                        $"{UcmSettings.SessionSettings.host}:{UcmSettings.SessionSettings.ucmPort}/api")
                    {
                        MaxTimeout = -1,
                        RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true
                    };

                var client = new RestClient(options);


                // Disable SSL checks as UCMSettings does not have a SSL cert by default

                var request = new RestRequest();
                request.AddHeader("Content-Type", "application/json;charset=UTF-8");
                request.AddHeader("Connection", "close");

                // Serialize the data request into a json and add it as body
                request.AddJsonBody(updateInboundRoutesRequest);
                var response = client.PostAsync(request);
                var updateInboundRoutesResponse =
                    JsonConvert.DeserializeObject<UpdateInboundRoutesResponse.Root>(response.Result.Content);
                if (updateInboundRoutesResponse.status == 0)
                {
                    return true;
                }

                Helper.WriteLogToEvents(
                    $"SetInboundMutiMode(); updateInboundRoutesResponse was {JsonConvert.SerializeObject(updateInboundRoutesResponse)}");
                return false;
            }
            catch (Exception e)
            {
                Helper.WriteLogToEvents($"SetInboundMutiMode(); {e.Message}");
                return false;
            }
        }


        public async Task<bool> SetInboundRouteMode1(string cookie, int routeIndex, string? destinationType = null,
            string? externalNumber = null, string? account = null)
        {
            // Default to 0600000000 on null
            externalNumber ??= "0600000000";

            if (destinationType == "account" && account == null)
            {
                var VMAccounts = await _accountsService.GetVoicemailAccountsAsync(cookie);
                if (VMAccounts != null && VMAccounts.Any())
                {
                    account = VMAccounts.First().extension;
                }
                else
                {
                    return false;
                }
            }

            try
            {
                var multimode = new List<UpdateInboundRoutesRequest.MultiMode>();
                var mode1 = new UpdateInboundRoutesRequest.MultiMode
                {
                    mode = "1", // We store the external number in mode 1 so always edit mode1
                    destination_type = destinationType, // We always set mode 1 to external number
                    external_number = externalNumber,
                    account = account
                };

                multimode.Add(mode1);
                var updateInboundRoutesRequest = new UpdateInboundRoutesRequest.Root
                {
                    request = new UpdateInboundRoutesRequest.Request
                    {
                        inbound_route = routeIndex,
                        action = "updateInboundRoute",
                        cookie = cookie,
                        multi_mode = multimode
                    }
                };

                var options =
                    new RestClientOptions(
                        $"{UcmSettings.SessionSettings.host}:{UcmSettings.SessionSettings.ucmPort}/api")
                    {
                        MaxTimeout = -1,
                        RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true
                    };

                var client = new RestClient(options);


                // Disable SSL checks as UCMSettings does not have a SSL cert by default

                var request = new RestRequest();
                request.AddHeader("Content-Type", "application/json;charset=UTF-8");
                request.AddHeader("Connection", "close");

                // Serialize the data request into a json and add it as body
                request.AddJsonBody(updateInboundRoutesRequest);
                var response = client.PostAsync(request);
                var updateInboundRoutesResponse =
                    JsonConvert.DeserializeObject<UpdateInboundRoutesResponse.Root>(response.Result.Content);
                if (updateInboundRoutesResponse.status == 0)
                {
                    return true;
                }

                Helper.WriteLogToEvents(
                    $"SetInboundRouteMode1(); updateInboundRoutesResponse was {JsonConvert.SerializeObject(updateInboundRoutesResponse)}");
                return false;
            }
            catch (Exception e)
            {
                Helper.WriteLogToEvents($"SetInboundRouteMode1(); {e.Message}");
                return false;
            }
        }
    }
}