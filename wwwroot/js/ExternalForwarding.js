﻿// Use the module syntax to export the function
function setMobileTable() {
    // if (window.innerWidth > 600) return false;
    const tableEl = document.querySelector("table");
    const thEls = tableEl.querySelectorAll('thead th');
    const tdLabels = Array.from(thEls).map(el => el.innerText);
    tableEl.querySelectorAll('tbody tr').forEach(tr => {
        Array.from(tr.children).forEach(
            (td, ndx) => td.setAttribute('label', tdLabels[ndx])
        );
    });
}


function tableAnimations() {
    // @media screen and (max-width: 800px) 
    if (document.documentElement.clientWidth < 800) {
        $("table > tbody  > tr").each(function () {
            $(this).dreyanim({
                animationType: 'fallInAlternate',
                animationTime: 500
            });
            $(this).delay(5000);
        });
    }
}
