﻿const contactPickerSupported = ('contacts' in navigator && 'ContactsManager' in window);

function checkAndGetContactFromPickerApiJS(objectReference) {
    if (contactPickerSupported) {
        // Show button for contacts
        getContacts(objectReference);
    } else {
        objectReference.invokeMethodAsync('GettingContactsNotSupported');
    }
}


async function getContacts(objectReference) {
    const props = [];
    props.push('tel');
    const opts = { multiple: false };

    try {
        const contacts = await navigator.contacts.select(props, opts);
        handleResults(objectReference, contacts);
    } catch (ex) {
        objectReference.invokeMethodAsync('GettingContactsSomethingWentWrong', ex);
    }

}

function handleResults(objectReference, contacts) {
    // We only request a single contact (see getContacts#R3)
    contacts.forEach((contact) => {
        objectReference.invokeMethodAsync('GettingContactsProcessTel', contact.tel.toString());
    });
}