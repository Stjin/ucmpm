using System;
using System.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using UCMPM.Data;

namespace UCMPM
{
    public class Program
    {
        public static void Main(string[] args)
        {

            // Write to EventLog to check if permissions are set
            if (Helper.WriteLogToEvents("Main(); Starting UCMPM...", EventLogEntryType.Information))
            {
                Console.BackgroundColor = ConsoleColor.Green;
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("Good job! You followed the manual! You can close me now and go ahead and run me through IIS.");
            }
            else
            {
                Console.BackgroundColor = ConsoleColor.Red;
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("Hey you!! You did not follow the manual. Run me as an administrator to let me register myself to the eventLogs!");

            }

            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args)
        {
            return Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder => { webBuilder.UseStartup<Startup>(); });
        }
    }
}