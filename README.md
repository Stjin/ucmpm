﻿#### This readme was last updated on 14-01-2023 (dd-MM-yyyy)

> Notes
>
>> :wrench: The latest tests of this application were done on a Grandstream UCM6301 with firmware 1.0.19.9 (UCM6301) // 1.0.20.38 (UCM6202). Future firmware versions ~~*might*~~ **will** break this application.

>> :exclamation: I (Stjin) noticed that the UCM6202 and 6301 return different data on the *listInboundRoute* endpoint, I have added extra code to make sure the did_pattern_match always contains data and have asked Grandstream why there is a difference in API between these devices/firmwares.


# UCMPM
An ASP.net, Blazor powered [API](http://www.grandstream.com/sites/default/files/Resources/UCM_API_Guide.pdf) wrapper for the Grandstream IP PBX Appliances. It's primary goal is to let customers switch inbound-route modes and change the external_number.

## Features

### Query setup (secure and unsecure)
> :warning: If you decide to use Query Setup, please avoid using URI query characters (such as the ?, & and ;) in the password as this might confuse the application

##### Secure (recommended)
When entering and filling in the profile field in the /settings page you can press the QR button to generate a QR and url containing the login information as an encrypted string. These URL's are valid for exactly 60 minutes making them secure to share.

##### Insecure
If you don't like security or want a fallback method, query strings could also be made containing all the settings in plain text. Making query strings is as easy as:

> http://ucmpm.website.com/settings?host=[HOSTNAME]&port=[PORT]&username=[USERNAME]&password=[PASSWORD]

An example would be:

    http://ucmpm.website.com/settings?host=https://office.contoso.com&port=18089&username=cdrapi&password=A123456!

*obviously you won't use A123456! as a password because you care about security*

As soon as the application detects a query string it will apply the settings and clean up the URL.

### Switch inbound route modes
The main goal of this application is giving users the ability to switch the inbound-route mode as an easy way of forwarding call to an external number or an account (which in the use case of this application is being used as voicemail).
*To prevent confusion when reading through the code, The workflow for which this application is built uses accounts (extensions) that are used as voicemails. An account / extension that contains a voicemail message. In this code accounts are most likely being filtered on accounts which fullname is starting with "VM-"*

### Change destination_type of mode1 (to account or external_number)
Users can easily change the destination_type of mode1 of a particular route to either account or external_number, in addition users can then select an account from a dropdown or type in an external number

### Multilanguage
This project is available in Dutch and English but defaults to dutch if the browser language does not match any of the languages (Startup.cs:52)

### Contact Picker API
The forwarding page of UCMPM has the Contact Picker API integrated, allowing users with a supported device/browser to select a number from their contacts app and use this.

### Customization

##### Custom branding
After installing UCMPM feel free to replace _branding.png_ in the wwwroot\modifyme folder with an image of choice. You can also delete the default image to hide it.

## Setup

### Configure your webserver
 1. Make sure you are running IIS
 2. Install the latest [.NET 7 Hosting Bundle](https://dotnet.microsoft.com/download/dotnet)
 3. Create a new site pointing to the directory with the published project
 4. Set the Application Pool's CLR version to "No Managed Code"
 5. Make sure to run and close UCMPM.exe as administrator **at least once!** (to initialize and register EventLogs)
 6. Redirect all http traffic to https using [this](https://www.namecheap.com/support/knowledgebase/article.aspx/9953/38/iis-redirect-http-to-https/) guide (*optional*)

### Configure your Grandstream UCM

##### Set up API access
 1. Make sure your UCM is **remotely** available through port forwarding or open ports. (The port to forward is 8089 as of writing)
 2. In the UCM, goto **API Configuration** under **Value-added Features**
 3. Enable HTTPS API and configure a secure username **(without special characters)** and password (Call control **does not** have to be checked)
 4. Add the IP address from the [webserver](#Configure-your-webserver) to the permitted IPs
 5. In the UCM, goto the **Login Security** tab in the **Login Settings** under **Maintenance**
 6. Add the IP address from the [webserver](#Configure-your-webserver) to the whitelist to prevent any lockouts by failed logins
 7. Check if you can make a connection through the UCMPM's settings page

*It should end up looking like this:*
![UCM HTTPS API settings](docs/ucm.png)

##### Set up inbound route
 1. Under **Inbound Routes** Enable **Inbound Multiple Mode** and make sure to have at least 1 Mode (which is set by default)
 2. Edit the inbound route that you want to enable the use of UCMPM for
 3. In the inbound rule settings, enable **Enable Route-Level Inbound Mode** and **Inbound Multiple Mode**
 4. The **Inbound Mode Suffix** can be set to anything that does not represent a phone number (eg. 999)
 5. Check if you can toggle the forwarding in UCMPM's external forwarding page

*It should end up looking like this:*
![UCM HTTPS API settings](docs/ucm_1.png)

## Updating
 1. Stop the IIS site
 2. Delete all the files in the websitefolder **except** the following files/folders
 
```
appsettings.json (contains settings related to your specific UCMPM instance)
wwwroot\modifyme folder (contains customizations)
web.config (contains specific server configuration)
```

 3. Extract the contents of the new version of UCMPM in the folder **(not replacing any existing files unless stated differently in the release changelog)**
 4. Start the IIS site

## Security
As UCM requires username/password to authenticate, settings will be stored in localstorage of the browser as plaintext meaning that the data is as secure as the device it is stored on.
I am looking into possibilities for storing this information on a more secure way.

## Troubleshooting

##### The changes could not be applied (-45 and -46)
A known restriction of the UCM API is that changes can only be applied once every 15 seconds. Changing multiple settings in a short interval will return in a "The changes could not be applied" error being displayed
UCMPM always tries to revert any changes done. This message is most likely going hand-in-hand with a -45 or -46 error code (See [Other errors](#Other-errors)).
There is absolutely nothing I can do about this as this is a restriction on the API-side. Building a queue is not on the roadmap.

##### Other errors
UCMPM logs all the errors and exceptions in Windows EventLog under the **Application** logs with **EventID 5151**
If you do not see any exceptions appearing there and you get an error message when starting UCMPM, congrats. You did not read the installation manual.

## Support-agents notes

##### Adding an inbound route / making an inbound route available for UCMPM
Please see [Section Setup-Set up inbound route](#Set-up-inbound-route)

##### Adding a account-voicemail / making a account-voicemail selectable in UCMPM
1. Create a new extension/account *Make sure the first name of the account starts with "VM-" to make the specific account visible in the routes overview**
2. Note the accountnumber and password for this extension/account
3. Logout of UCM
4. Login as this account using the account number and password
5. Configure the voicemail settings in Personal-data=>Voicemail
6. Configure any email settings or other configurations _optional_


# Licenses
This project is licensed under

[![License: CC BY-ND 4.0](https://img.shields.io/badge/License-CC%20BY--ND%204.0-lightgrey.svg)](https://creativecommons.org/licenses/by-nd/4.0/)

## Libraries
* [QRCoder](https://github.com/codebude/QRCoder)
* [Newtonsoft.Json](https://github.com/JamesNK/Newtonsoft.Json)
* [RestSharp](https://github.com/restsharp/RestSharp)
* [Blazored.LocalStorage](https://github.com/Blazored/LocalStorage)
* [Radzen Blazor](https://github.com/radzenhq/radzen-blazor)
* [JsConsole](https://github.com/dochoss/JsConsole)