﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.WebUtilities;
using Radzen;
using UCMPM.Data;
using UCMPM.Shared;


namespace UCMPM.Pages
{
    public partial class Settings
    {
        EventConsole? _console;

        public Ucm TempUcmSettings = new();

        protected override async Task OnInitializedAsync()
        {
            // Reload the values in memory
            //await UcmSettings.LoadUcmSettingsToMemory(LocalStorage);

            // Check if credentials are being passed
            if (await CheckForUrlParameters())
            {
                NavigationManager.NavigateTo(_navigateTo != null ? $"/{_navigateTo}" : "/settings");
            }
            else
            {
                // Load the settings into the tempsettings
                TempUcmSettings = UcmSettings.SessionSettings;
            }
        }

        void ShowUsernameTooltip(ElementReference elementReference, TooltipOptions? options = null) =>
            tooltipService.Open(elementReference, LanguageContainer["usernameSettingsNoteToolTip"], options);


        void Submit()
        {
            TestConnection();
        }

        protected async void TestConnection()
        {
            // Write tempUCM to localStorage (which reads data to memory)
            if (!await UcmSettings.SetUcmSettingsAsync(LocalStorage, TempUcmSettings)) return;
            _console?.Log(LanguageContainer["settingsSaved"]);
            _console?.Log(LanguageContainer["testingConnection", UcmSettings.SessionSettings.host,
                UcmSettings.SessionSettings.ucmPort]);

            var authenticate = new Authenticate(UcmSettings);

            // Authenticate on the UCM, and return a string containing the status ("0" if it succeeded)
            var connectStatus = await authenticate.AuthenticateOnUCMAsync(LocalStorage);

            // If connectStatus is "0", that means that the authentication on the UCM was successful thus we can continue obtaining the cookie (yum)
            if (connectStatus == "0")
            {
                // Obtain (and wait for) the cookie
                var cookie = await authenticate.GetCookieAsync(LocalStorage);


#if DEBUG
                _console?.Log($"DEBUG: COOKIE IS {cookie}");
#endif

                // Get some system information so the user knows the connnection is working
                var systemStatus = await SystemStatusService.ObtainSystemStatus(cookie);

                // Show a message letting the user know the connection succeeded
                var successMessage = new NotificationMessage
                {
                    Severity = NotificationSeverity.Success,
                    Summary = LanguageContainer["settings"],
                    Detail = LanguageContainer["connectionToUCMSucceeded"],
                    Duration = 4000
                };
                NotificationService.Notify(successMessage);

                // Show the systemInfo in the console
                _console?.Log(LanguageContainer["connectedToUCM", systemStatus.SerialNumber, systemStatus.UpTime]);
            }
            else
            {
                var errorMessage = new NotificationMessage
                {
                    Severity = NotificationSeverity.Error,
                    Summary = LanguageContainer["settings"],
                    Detail = LanguageContainer["connectionToUCMFailed"],
                    Duration = 4000
                };
                NotificationService.Notify(errorMessage);

                // Show the particular error in the console (I only make user errors visible on this page)
                _console?.Log(LanguageContainer["errorWhileConnecting", connectStatus]);
            }
        }


        /*
        OBTAIN SETTINGS THROUGH PARAMETERS
        */
        // Holds the page to navigate to when finished
        private string? _navigateTo;

        private async Task<bool> CheckForUrlParameters()
        {
            var uri = NavigationManager.ToAbsoluteUri(NavigationManager
                .Uri); //you can use IURIHelper for older versions
            var configurationChanged = false;

            // If there is a query called secret that means that a QR code (which contains the secret query) was scanned.
            // Skip all the other queries and parse the secret query
            if (QueryHelpers.ParseQuery(uri.Query).TryGetValue("secret", out var secret))
            {
                try
                {
                    var qrCodeString = StringCipher.Decrypt(secret);
                    var qrCodeParams = qrCodeString.Split("||");

                    bool shouldReadSettings;
                    // If the array has more than 5 elements (6), it contains the expireTime, check for that first.
                    if (qrCodeParams.Length > 5)
                    {
                        // First check the expiretime before assigning values
                        long expireTime = Convert.ToInt64(qrCodeParams[5]);

                        var currentTime = DateTime.Now;
                        long currentTimeMilliseconds = new DateTimeOffset(currentTime).ToUnixTimeMilliseconds();

                        shouldReadSettings = expireTime > currentTimeMilliseconds;
                    }
                    // If it does not. Start reading as its probably a permanent QR
                    else
                    {
                        shouldReadSettings = true;
                    }


                    if (shouldReadSettings)
                    {
                        TempUcmSettings.host = qrCodeParams[0];
                        TempUcmSettings.ucmPort = qrCodeParams[1];
                        TempUcmSettings.username = qrCodeParams[2];
                        TempUcmSettings.password = qrCodeParams[3];
                        _navigateTo = qrCodeParams[4];
                        configurationChanged = true;
                    }
                    else
                    {
                        var errorMessage = new NotificationMessage
                        {
                            Severity = NotificationSeverity.Error, Summary = LanguageContainer["settings"],
                            Detail = LanguageContainer["qrCodeExpired"], Duration = 4000
                        };
                        NotificationService.Notify(errorMessage);
                    }
                }
                catch (Exception e)
                {
                    Helper.WriteLogToEvents($"CheckForUrlParameters(); {e.Message}");
                    var errorMessage = new NotificationMessage
                    {
                        Severity = NotificationSeverity.Error, Summary = LanguageContainer["settings"],
                        Detail = LanguageContainer["qrCodeNotValid"], Duration = 4000
                    };
                    NotificationService.Notify(errorMessage);
                    return false;
                }
            }
            else
            {
                if (QueryHelpers.ParseQuery(uri.Query).TryGetValue("host", out var host))
                {
                    TempUcmSettings.host = host.First();
                    configurationChanged = true;
                }

                if (QueryHelpers.ParseQuery(uri.Query).TryGetValue("port", out var port))
                {
                    TempUcmSettings.ucmPort = port.First();
                    configurationChanged = true;
                }

                if (QueryHelpers.ParseQuery(uri.Query).TryGetValue("username", out var username))
                {
                    TempUcmSettings.username = username.First();
                    configurationChanged = true;
                }

                if (QueryHelpers.ParseQuery(uri.Query).TryGetValue("password", out var password))
                {
                    TempUcmSettings.password = password.First();
                    configurationChanged = true;
                }

                if (QueryHelpers.ParseQuery(uri.Query).TryGetValue("navigate", out var navigate))
                {
                    _navigateTo = navigate;
                    configurationChanged = true;
                }
            }

            // If any of the settings are changed, store the UCM object into localstorage, show a notification to notify the user and return true
            if (!configurationChanged) return false;
            // Write tempUCM to localStorage (which reads data to memory)
            if (!await UcmSettings.SetUcmSettingsAsync(LocalStorage, TempUcmSettings)) return false;
            var message = new NotificationMessage
            {
                Severity = NotificationSeverity.Info, Summary = LanguageContainer["settingsImportedQrURL"],
                Duration = 4000
            };
            NotificationService.Notify(message);
            return true;
        }


        async Task CopyToClipboard(string fullSecretString)
        {
            await ClipboardService.WriteTextAsync(fullSecretString);
            var message = new NotificationMessage
            {
                Severity = NotificationSeverity.Info, Summary = LanguageContainer["urlCopiedToClipboard"],
                Duration = 4000
            };
            NotificationService.Notify(message);
        }


        async Task OnChangeHideNotConfiguredRoutes()
        {
            await UcmSettings.SetPersonalUcmSettingsAsync(LocalStorage, UcmSettings.PersonalSessionSettings);
        }
    }
}