﻿#nullable enable
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using Radzen;
using UCMPM.Data;

namespace UCMPM.Pages
{
    public partial class ExternalForwarding
    {
        private List
            <ListInboundRoutesResponse.InboundRoute>? _inboundRoutes;

        private List
            <ListAccountsResponse.Account>? _accounts;

        private Authenticate _authenticate;

        private string _externalNumber = "";

        // This value keeps track of the amount of routes that match forwarding requirements.
        private int _amountOfRoutesThatMatchRequirement = 0;


        //https://blazor-university.com/javascript-interop/calling-dotnet-from-javascript/lifetimes-and-memory-leaks/
        // This is used to call and call-back to and from javascript and dotnet
        private DotNetObjectReference<ExternalForwarding>? _objectReference;


        void ShowSetForwardingNotification(bool switchDisabledStatus)
        {
            if (!switchDisabledStatus) return;
            var message = new NotificationMessage
            {
                Severity = NotificationSeverity.Info,
                Summary = LanguageContainer["forwardingNotPossibleNotif"],
                Duration = 5000
            };
            NotificationService.Notify(message);
        }


        // ENABLE/DISABLE mode1
        async Task OnChange(bool? value, ListInboundRoutesResponse.InboundRoute route)
        {

            // Check if the external number was entered! (Only perform this check on enabling the mode1)
            if (value == true)
            {
                // The inboundroute members contain all the members (including the specific time and holiday members)
                // The Mode1 default destination is the 2nd elements that has the value "tc": "0",
                // So use find to get all the members where tc is 0, and THEN pick the 2nd item from the list
                List<ListInboundRoutesResponse.Member>
                    routeMembers = route.members.Where(i => i.tc == "0").ToList(); // C# 3.0+

                // If the route is set to external number
                if (routeMembers[1].destination_type == "external_number")
                {
                    // If the external number is empty
                    if (string.IsNullOrEmpty(routeMembers[1].external_number))
                    {
                        // Show error and return
                        NoNumberEnteredError();
                        return;
                    }
                }

                // If the route is set to external number
                else if (routeMembers[1].destination_type == "account")
                {
                    // If the external number is empty
                    if (string.IsNullOrEmpty(routeMembers[1].account))
                    {
                        // Show error and return
                        NoVoicemailEnteredError();
                        return;
                    }
                }
            }


            BusyDialog(LanguageContainer["changingForwardNotif"]);
            
            var mode = 0;
            if (value == true)
            {
                mode = 1;
            }


            var cookie = await _authenticate.GetCookieAsync(LocalStorage);

            // Set inboundmode and check if succeeded
            Helper.WriteLogToEvents(
                $"OnChange(); About to switch mode1 for route {route.inbound_rt_index} with currentInboundMode being {route.inbound_mode} for host {_authenticate.UcmSettings.SessionSettings.host} to {value}",
                EventLogEntryType.Information);
            var setInboundMutiModeStatus =
                InboundRoutesService.SetInboundMutiMode(cookie, mode, route.inbound_rt_index);
            if (setInboundMutiModeStatus)
            {
                // Reload the data to update the switch for the user
                GetInboundRoutes();

                Helper.WriteLogToEvents(
                    $"OnChange(); Switching mode1 for route {route.inbound_rt_index} for host {_authenticate.UcmSettings.SessionSettings.host} to {value} was a success!",
                    EventLogEntryType.Information);
                var applyChangesStatus = await ApplyChangesService.ApplyChangesAsync(cookie);
                if (applyChangesStatus)
                {
                    DialogService.Close();
                    NotificationService.Messages.Clear();

                    var successMessage = new NotificationMessage
                    {
                        Severity = NotificationSeverity.Success,
                        Summary = LanguageContainer["forwarding"],
                        Detail = LanguageContainer["forwardingSucceeded"],
                        Duration = 4000
                    };
                    NotificationService.Notify(successMessage);
                }
                else
                {
                    // The ApplyChanges endpoint is most likely hitting its every-15-second limit. Revert the change made and notify the user.
                    var revertSetInboundMutiModeStatus =
                        InboundRoutesService.SetInboundMutiMode(cookie, route.inbound_mode, route.inbound_rt_index);
                    if (revertSetInboundMutiModeStatus)
                    {
                        DialogService.Close();
                        NotificationService.Messages.Clear();

                        var errorMessage = new NotificationMessage
                        {
                            Severity = NotificationSeverity.Error,
                            Summary = LanguageContainer["forwarding"],
                            Detail = LanguageContainer["forwardingFailedRevertedChanges"],
                            Duration = 4000
                        };
                        NotificationService.Notify(errorMessage);
                    }
                    else
                    {
                        DialogService.Close();
                        NotificationService.Messages.Clear();

                        // What the hell? Somehow can't change the route's inbound mode back to what it was. Let the user know!!.
                        var errorMessage = new NotificationMessage
                        {
                            Severity = NotificationSeverity.Error,
                            Summary = LanguageContainer["forwarding"],
                            Detail = LanguageContainer["forwardingFailedCouldNotRevertChanges"],
                            Duration = 4000
                        };
                        NotificationService.Notify(errorMessage);
                    }
                }
            }
            else
            {
                DialogService.Close();
                NotificationService.Messages.Clear();

                var errorMessage = new NotificationMessage
                {
                    Severity = NotificationSeverity.Error,
                    Summary = LanguageContainer["forwarding"],
                    Detail = LanguageContainer["forwardingFailed"],
                    Duration = 4000
                };
                NotificationService.Notify(errorMessage);
            }

            // No matter what happened above, always reload the data
            GetInboundRoutes();
        }


        private int _renderTimes;

        protected override void OnAfterRender(bool firstRender)
        {
            // To prevent animations from replaying too much, only play them twice
            if (_renderTimes < 3)
            {
                AnimateMobileUi();
            }

            SetMobileFriendlyUi();
            _renderTimes++;
        }

        protected override async Task OnInitializedAsync()
        {
            _authenticate = new Authenticate(UcmSettings);

            // Obtain a cookie
            if (!await ObtainCookie())
            {
                var errorMessage = new NotificationMessage
                {
                    Severity = NotificationSeverity.Error,
                    Summary = LanguageContainer["settings"],
                    Detail = LanguageContainer["connectionToUCMFailed"],
                    Duration = 4000
                };
                NotificationService.Notify(errorMessage);
            }
            else
            {
                // Obtain accounts (extensions)
                await GetAccounts();

                // Obtain the inbound routes
                await GetInboundRoutes();

                StateHasChanged();
            }
        }

        private async Task<bool> ObtainCookie()
        {
            // Authenticate on the UCM, and return a boolean containing the status (if it succeeded or not)
            var connectStatus = await _authenticate.AuthenticateOnUCMAsync(LocalStorage);

            // If readyToConnect is "0", that means that the authentication on the UCM was successful thus we have obtained the cookie (yum)
            return connectStatus == "0";
        }

        private async Task GetInboundRoutes()
        {
            //Helper.WriteLogToEvents($"GetInboundRoutes(); About to obtain inbound routes for host {_authenticate.UcmSettings.SessionSettings.host}", EventLogEntryType.Information);

            // Obtain (and wait for) the cookie
            var cookie = await _authenticate.GetCookieAsync(LocalStorage);

            // Get the inboundroutes (pass the cookie to authenticate with through)
            _inboundRoutes = await InboundRoutesService.GetInboundRoutesAsync(cookie);

            StateHasChanged();
        }

        private async Task GetAccounts()
        {
            //Helper.WriteLogToEvents($"GetInboundRoutes(); About to obtain accounts for host {_authenticate.UcmSettings.SessionSettings.host}", EventLogEntryType.Information);

            // Obtain (and wait for) the cookie
            var cookie = await _authenticate.GetCookieAsync(LocalStorage);

            // Get the accounts (extension) (pass the cookie to authenticate with through)
            _accounts = await AccountsService.GetVoicemailAccountsAsync(cookie);
            StateHasChanged();
        }


        // Set account of a route
        async Task OnChangeAccount(object value, ListInboundRoutesResponse.InboundRoute route)
        {
            object str = value is IEnumerable<object> objects ? string.Join(", ", objects) : value;

            //Console.WriteLine($"Set inbound route {route.inbound_rt_index} to account {str}");

            BusyDialog(LanguageContainer["changingForwardAccountNotif"]);

            var cookie = await _authenticate.GetCookieAsync(LocalStorage);

            // Set account and check if succeeded
            Helper.WriteLogToEvents(
                $"OnChangeAccount(); About to change the account for route {route.inbound_rt_index} ({route.inbound_rt_name}) for host {_authenticate.UcmSettings.SessionSettings.host} to {str}",
                EventLogEntryType.Information);
            var setInboundRouteMode1Status =
                await InboundRoutesService.SetInboundRouteMode1(cookie, route.inbound_rt_index, "account", null,
                    str.ToString());
            if (setInboundRouteMode1Status)
            {
                Helper.WriteLogToEvents(
                    $"OnChangeAccount(); Changing the account for route {route.inbound_rt_index} ({route.inbound_rt_name}) for host {_authenticate.UcmSettings.SessionSettings.host} to {str} was a success!",
                    EventLogEntryType.Information);

                // If the inbound_mode is not 0, this means that the inbound mode is set to something else than default thus is in active use.
                if (route.inbound_mode != 0)
                {
                    // In this case, apply the changes
                    var applyChangesStatus = await ApplyChangesService.ApplyChangesAsync(cookie);
                    if (applyChangesStatus)
                    {
                        DialogService.Close();
                        NotificationService.Messages.Clear();

                        var successMessage = new NotificationMessage
                        {
                            Severity = NotificationSeverity.Success,
                            Summary = LanguageContainer["forwarding"],
                            Detail = LanguageContainer["forwardingAccountSucceeded"],
                            Duration = 4000
                        };
                        NotificationService.Notify(successMessage);
                    }
                    else
                    {
                        // The ApplyChanges endpoint is most likely hitting its every-15-second limit. Revert the change made and notify the user.
                        var revertSetInboundRouteMode1Status =
                            await InboundRoutesService.SetInboundRouteMode1(cookie, route.inbound_rt_index,
                                route.members?[1].destination_type, route.members?[1].external_number,
                                route.members?[1].account);
                        if (revertSetInboundRouteMode1Status)
                        {
                            DialogService.Close();
                            NotificationService.Messages.Clear();

                            var errorMessage = new NotificationMessage
                            {
                                Severity = NotificationSeverity.Error,
                                Summary = LanguageContainer["forwarding"],
                                Detail = LanguageContainer["forwardingAccountFailedRevertedChanges"],
                                Duration = 4000
                            };
                            NotificationService.Notify(errorMessage);
                        }
                        else
                        {
                            DialogService.Close();
                            NotificationService.Messages.Clear();

                            // What the hell? Somehow can't change the route back to what it was. Let the user know!!
                            var errorMessage = new NotificationMessage
                            {
                                Severity = NotificationSeverity.Error,
                                Summary = LanguageContainer["forwarding"],
                                Detail = LanguageContainer["forwardingAccountFailedCouldNotRevertChanges"],
                                Duration = 4000
                            };
                            NotificationService.Notify(errorMessage);
                        }
                    }
                }
                else
                {
                    // If the inbound_mode is 0, this means that the inbound mode is set to default mode.
                    // Since the default mode is enabled, applying the changes does not make sense as it won't have any useful effect
                    // It would just make the process painfully slow as you would hit the 15-second API limit since most people would change the inbound_muti_mode anyways. (Which does apply changes)

                    DialogService.Close();
                    NotificationService.Messages.Clear();

                    var successMessage = new NotificationMessage
                    {
                        Severity = NotificationSeverity.Success,
                        Summary = LanguageContainer["forwarding"],
                        Detail = LanguageContainer["forwardingAccountSucceeded"],
                        Duration = 4000
                    };
                    NotificationService.Notify(successMessage);
                }
            }
            else
            {
                DialogService.Close();
                NotificationService.Messages.Clear();

                var errorMessage = new NotificationMessage
                {
                    Severity = NotificationSeverity.Error,
                    Summary = LanguageContainer["forwarding"],
                    Detail = LanguageContainer["forwardingAccountFailed"],
                    Duration = 4000
                };
                NotificationService.Notify(errorMessage);
            }

            // No matter what happened above, always reload the data
            OnInitializedAsync();
        }

        void NoNumberEnteredError()
        {
            var errorMessage = new NotificationMessage
            {
                Severity = NotificationSeverity.Warning,
                Summary = LanguageContainer["noNumberEntered"],
                Detail = LanguageContainer["noNumberEnteredDesc"],
                Duration = 4000
            };
            NotificationService.Notify(errorMessage);
        }

        void NoVoicemailEnteredError()
        {
            var errorMessage = new NotificationMessage
            {
                Severity = NotificationSeverity.Warning,
                Summary = LanguageContainer["noVoicemailEntered"],
                Detail = LanguageContainer["noVoicemailEnteredDesc"],
                Duration = 4000
            };
            NotificationService.Notify(errorMessage);
        }

        async Task BusyDialog(string message)
        {
            await DialogService.OpenAsync("", ds =>
            {
                RenderFragment content = b =>
                {
                    b.OpenElement(0, "div");
                    b.AddAttribute(1, "class", "row");

                    b.OpenElement(2, "div");
                    b.AddAttribute(3, "class", "col-md-12");

                    b.AddContent(4, message);

                    b.CloseElement();
                    b.CloseElement();
                };
                return content;
            }, new DialogOptions() { ShowTitle = false, Style = "min-height:auto;min-width:auto;width:auto;margin: auto;width: 60 %;border: 5px solid #05587E;padding: 10px;" });
        }


        // Set target for inbound route
        async Task OnChangeTarget(object value, ListInboundRoutesResponse.InboundRoute route)
        {
            object str = value is IEnumerable<object> objects ? string.Join(", ", objects) : value;

            //Console.WriteLine($"Set inbound route {route.inbound_rt_index} to destination_type {str}");


            BusyDialog(LanguageContainer["changingDestinationTypeNotif"]);

            var cookie = await _authenticate.GetCookieAsync(LocalStorage);

            // Set target and check if succeeded
            Helper.WriteLogToEvents(
                $"OnChangeTarget(); About to change the target for route {route.inbound_rt_index} ({route.inbound_rt_name}) for host {_authenticate.UcmSettings.SessionSettings.host} to {str}",
                EventLogEntryType.Information);
            var setInboundRouteMode1Status =
                await InboundRoutesService.SetInboundRouteMode1(cookie, route.inbound_rt_index, str.ToString(), null, null);
            if (setInboundRouteMode1Status)
            {
                Helper.WriteLogToEvents(
                    $"OnChangeTarget(); Changing the target for route {route.inbound_rt_index} ({route.inbound_rt_name}) for host {_authenticate.UcmSettings.SessionSettings.host} to {str} was a success!",
                    EventLogEntryType.Information);

                DialogService.Close();
                NotificationService.Messages.Clear();

                // Do not apply settings here, the user still has to select a target destination which will trigger applyChanges
                var successMessage = new NotificationMessage
                {
                    Severity = NotificationSeverity.Success,
                    Summary = LanguageContainer["forwarding"],
                    Detail = LanguageContainer["forwardingDestinationTypeSucceeded"],
                    Duration = 4000
                };
                NotificationService.Notify(successMessage);
            }
            else
            {
                DialogService.Close();
                NotificationService.Messages.Clear();

                var errorMessage = new NotificationMessage
                {
                    Severity = NotificationSeverity.Error,
                    Summary = LanguageContainer["forwarding"],
                    Detail = LanguageContainer["forwardingDestinationTypeFailed"],
                    Duration = 4000
                };
                NotificationService.Notify(errorMessage);
            }

            // No matter what happened above, always reload the data
            OnInitializedAsync();
        }

        // Set external number for inbound route
        private async Task OnClickSaveExternalNumber(string externalNumber,
            ListInboundRoutesResponse.InboundRoute route)
        {
            externalNumber = Helper.FixNumberSyntax(externalNumber);
            if (Helper.ValidatePhone(externalNumber))
            {


                BusyDialog(LanguageContainer["changingForwardNumNotif"]);

                Helper.WriteLogToEvents(
                    $"OnClickSaveExternalNumber(); About to change the number for route {route.inbound_rt_index} ({route.inbound_rt_name}) for host {_authenticate.UcmSettings.SessionSettings.host} to {externalNumber}",
                    EventLogEntryType.Information);
                var cookie = await _authenticate.GetCookieAsync(LocalStorage);

                // Set inboundmode and check if succeeded
                var setInboundRouteMode1 =
                    await InboundRoutesService.SetInboundRouteMode1(cookie, route.inbound_rt_index, "external_number",
                        externalNumber, null);
                if (setInboundRouteMode1)
                {
                    // If the inbound_mode is not 0, this means that the inbound mode is set to something else than default thus is in active use.
                    if (route.inbound_mode != 0)
                    {
                        // In this case, apply the changes
                        var applyChangesStatus = await ApplyChangesService.ApplyChangesAsync(cookie);
                        if (applyChangesStatus)
                        {
                            DialogService.Close();
                            NotificationService.Messages.Clear();

                            var successMessage = new NotificationMessage
                            {
                                Severity = NotificationSeverity.Success,
                                Summary = LanguageContainer["forwarding"],
                                Detail = LanguageContainer["forwardingNumSucceeded"],
                                Duration = 4000
                            };
                            NotificationService.Notify(successMessage);
                        }
                        else
                        {
                            // The ApplyChanges endpoint is most likely hitting its every-15-second limit. Revert the change made and notify the user.
                            var revertSetInboundRouteMode1Status =
                                await InboundRoutesService.SetInboundRouteMode1(cookie, route.inbound_rt_index,
                                    route.members?[1].destination_type, route.members?[1].external_number,
                                    route.members?[1].account);

                            if (revertSetInboundRouteMode1Status)
                            {
                                DialogService.Close();
                                NotificationService.Messages.Clear();

                                var errorMessage = new NotificationMessage
                                {
                                    Severity = NotificationSeverity.Error,
                                    Summary = LanguageContainer["forwarding"],
                                    Detail = LanguageContainer["forwardingNumFailedRevertedChanges"],
                                    Duration = 4000
                                };
                                NotificationService.Notify(errorMessage);
                            }
                            else
                            {
                                DialogService.Close();
                                NotificationService.Messages.Clear();

                                // What the hell? Somehow can't change the route back to what it was. Let the user know!!
                                var errorMessage = new NotificationMessage
                                {
                                    Severity = NotificationSeverity.Error,
                                    Summary = LanguageContainer["forwarding"],
                                    Detail = LanguageContainer["forwardingNumFailedCouldNotRevertChanges"],
                                    Duration = 4000
                                };
                                NotificationService.Notify(errorMessage);
                            }
                        }
                    }
                    else
                    {
                        // If the inbound_mode is 0, this means that the inbound mode is set to default mode.
                        // Since the default mode is enabled, applying the changes does not make sense as it won't have any useful effect
                        // It would just make the process painfully slow as you would hit the 15-second API limit since most people would change the inbound_muti_mode anyways. (Which does apply changes)
                        DialogService.Close();
                        NotificationService.Messages.Clear();

                        var successMessage = new NotificationMessage
                        {
                            Severity = NotificationSeverity.Success,
                            Summary = LanguageContainer["forwarding"],
                            Detail = LanguageContainer["forwardingNumSucceeded"],
                            Duration = 4000
                        };
                        NotificationService.Notify(successMessage);
                    }

                    // No matter what happened above, always reload the data
                    OnInitializedAsync();
                }
                else
                {
                    DialogService.Close();
                    NotificationService.Messages.Clear();

                    var errorMessage = new NotificationMessage
                    {
                        Severity = NotificationSeverity.Error,
                        Summary = LanguageContainer["forwarding"],
                        Detail = LanguageContainer["forwardingNumFailed"],
                        Duration = 4000
                    };
                    NotificationService.Notify(errorMessage);
                }
            }
            else
            {
                var errorMessage = new NotificationMessage
                {
                    Severity = NotificationSeverity.Error,
                    Summary = LanguageContainer["forwarding"],
                    Detail = LanguageContainer["incorrectNumber"],
                    Duration = 4000
                };
                NotificationService.Notify(errorMessage);
            }
        }


        private void OnChangeExternalNumber(string args)
        {
            _externalNumber = args;
        }

        private string? FigureOutNumber(ListInboundRoutesResponse.InboundRoute route)
        {
            string number;
            if (!string.IsNullOrEmpty(route.inbound_rt_name))
            {
                number = route.inbound_rt_name;
            }
            else if (!string.IsNullOrEmpty(route.did_pattern_match))
            {
                number = route.did_pattern_match;
            }
            else if (!string.IsNullOrEmpty(route.did_pattern_match_list))
            {
                number = route.did_pattern_match_list;
            }
            else
            {
                return null;
            }

            // Replace _31 country code with 0 (in case we get a pattern)
            return number.Replace("_31", "0").Replace("_X.", LanguageContainer["allNumbers"]);
        }


        private string? FigureOutAccountExtensionFullname(string account)
        {
            return _accounts?.Where(kv => kv.extension == account)
                .Select(kv => kv.fullname) // not a problem even if no item matches
                .DefaultIfEmpty(LanguageContainer["unknownTarget"]) // or no argument -> null
                .First(); // cannot cause an exception
        }


        /* HERE IS THE PART FOR CHOOSING A CONTACT FROM THE CONTACTLIST */
        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            await base.OnAfterRenderAsync(firstRender);
            if (firstRender)
            {
                _objectReference = DotNetObjectReference.Create(this);
            }
        }

        private ListInboundRoutesResponse.InboundRoute? _routeIndexToChangeNumberToContactFrom;

        private async Task CheckAndGetContactFromPickerApi(ListInboundRoutesResponse.InboundRoute route)
        {
            _routeIndexToChangeNumberToContactFrom = route;
            if (_objectReference != null)
                await JsRuntime.InvokeVoidAsync("checkAndGetContactFromPickerApiJS", _objectReference);
        }

        [JSInvokable("GettingContactsNotSupported")]
        public void GettingContactsNotSupported()
        {
            GettingContactsNotSupportedNotification();
        }

        [JSInvokable("GettingContactsSomethingWentWrong")]
        public void GettingContactsSomethingWentWrong(string error)
        {
            GettingContactsSomethingWentWrongNotification(error);
        }

        [JSInvokable("GettingContactsProcessTel")]
        public void GettingContactsProcessTel(string tel)
        {
            GettingContactsProcessAndFillInTel(tel);
        }

        private void GettingContactsNotSupportedNotification()
        {
            var errorMessage = new NotificationMessage
            {
                Severity = NotificationSeverity.Warning,
                Summary = LanguageContainer["forwarding"],
                Detail = LanguageContainer["forwardingContactPickerApiNotSupported"],
                Duration = 4000
            };
            NotificationService.Notify(errorMessage);
        }

        private void GettingContactsProcessAndFillInTel(string tel)
        {
            try
            {
                var listOfNumbers = tel.Split(',').ToList();

                switch (listOfNumbers.Count)
                {
                    case 1:
                    {
                        var num = listOfNumbers[0];
                        if (_routeIndexToChangeNumberToContactFrom != null)
                            OnClickSaveExternalNumber(num, _routeIndexToChangeNumberToContactFrom);
                        break;
                    }
                    case > 1:
                        SelectNumberFromMultipleNumbers(listOfNumbers);
                        break;
                    default:
                    {
                        var errorMessage = new NotificationMessage
                        {
                            Severity = NotificationSeverity.Error,
                            Summary = LanguageContainer["forwarding"],
                            Detail = LanguageContainer["forwardingContactPickerApiNoValidNumber"],
                            Duration = 4000
                        };
                        NotificationService.Notify(errorMessage);
                        break;
                    }
                }
            }
            catch (Exception e)
            {
                Helper.WriteLogToEvents($"GettingContactsProcessAndFillInTel(); {e.Message}");
                var errorMessage = new NotificationMessage
                {
                    Severity = NotificationSeverity.Error,
                    Summary = LanguageContainer["forwarding"],
                    Detail = LanguageContainer["forwardingContactPickerApiError"],
                    Duration = 4000
                };
                NotificationService.Notify(errorMessage);
            }
        }

        private void SelectedNumberMultipleNumbersDialog(string value)
        {
            if (_routeIndexToChangeNumberToContactFrom != null)
                OnClickSaveExternalNumber(value, _routeIndexToChangeNumberToContactFrom);
        }

        private void GettingContactsSomethingWentWrongNotification(string error)
        {
            Helper.WriteLogToEvents($"GettingContactsSomethingWentWrongNotification(); {error}");

            var errorMessage = new NotificationMessage
            {
                Severity = NotificationSeverity.Error,
                Summary = LanguageContainer["forwarding"],
                Detail = LanguageContainer["forwardingContactPickerApiError"],
                Duration = 4000
            };
            NotificationService.Notify(errorMessage);
        }

        private async Task AnimateMobileUi()
        {
            await JsRuntime.InvokeAsync
                <object>
                ("tableAnimations");
        }

        private async Task SetMobileFriendlyUi()
        {
            await JsRuntime.InvokeAsync
                <object>
                ("setMobileTable");
        }

        private bool doesMatchForwardingRequirements(ListInboundRoutesResponse.InboundRoute route)
        {
            // This method is here to check if a route matches the requirements for forwarding. There are 4 requirements:
            // - A route needs to have members
            // - A route needs to have at least 1 member (even tho we only modify member1/mode1)
            // - A route needs to have en_multi_mode enabled
            // - A route needs to have enable_inbound_muti_mode enabled

            // If route.enable_inbound_muti_mode is "no", that means "Enable Route-Level Inbound Mode" is disabled (switching per-route modes is not possible)
            // If route.en_multi_mode is "no", that means "Inbound Multiple Mode" is disabled (multiple modes is not possible)
            return route.members != null && route.members.Count > 1 && route.en_multi_mode == "yes" &&
                   route.enable_inbound_muti_mode == "yes";
        }
    }
}